#! /bin/bash
#
# Build a typical dcentriq spring boot java application.
#

set -e

# import common functions
. $(dirname ${BASH_SOURCE[0]})/common.sh

phase=$(get_phase)
ver=$(get_version)

#mvn -B clean package
#app=$BITBUCKET_REPO_SLUG
#app=$(basename $(git config --get remote.origin.url))
app=$(basename $(git remote get-url origin) .git)
echo $app
build_docker_image "$app" "$phase" "$ver"