#
# $BITBUCKET_*:
#     These variables are provided by bitbucket pipeline.
#
# $DOCKER_*:
#     Docker registry credentials.
#     These are per repository variables set in bitbucket repository settings.
#

log() {
    >&2 echo "# $1"
}

get_branch_version() {
    # branch name should look something linke this: release/x.y.z
    # x, y and z should be numbers
  if [[ $BITBUCKET_BRANCH =~ ^release/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
      echo "${BASH_REMATCH[1]}"
      A="release"
  elif [[ $BITBUCKET_BRANCH =~ ^hotfix/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
     echo "${BASH_REMATCH[1]}"
     local B="hotfix"
  fi
}
get_phase() {
if [[ $BITBUCKET_BRANCH =~ ^release/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
    A="release"
   echo $A
elif [[ $BITBUCKET_BRANCH =~ ^hotfix/([0-9]+\.[0-9]+\.[0-9]+)$ ]]
  then
    B="hotfix"
    echo $B
elif [ $BITBUCKET_BRANCH == dev ]
   then
     echo "alpha"
fi
}
get_version() {
    if [[ $BITBUCKET_BRANCH == dev ]]; then
        get_source_version
    else
        v=$(get_branch_version)

        if [[ -z $v ]]; then
            log "invalid release branch name: $BITBUCKET_BRANCH"
            return 1
        else
            echo $v
        fi
    fi
}

build_docker_image() {
    local app=$1
    local phase=$2
    local ver=$3

    tag=$ver-$phase$BITBUCKET_BUILD_NUMBER.r${BITBUCKET_COMMIT:0:7}
    echo "new tag: $tag"

    name=$DOCKER_SERVER/$DOCKER_USERNAME/$app:$tag
    echo "new image: $name"

    docker login --username=$DOCKER_USERNAME --password=$DOCKER_PASSWORD
    docker build --no-cache -t "$name" .
    docker push "$name"
}
